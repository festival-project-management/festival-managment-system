<?php
// Database credentials
$hostname = "mysql.fsb.miamioh.edu";
$username = "musicfest";
$password = "fsb571musicfest";
$dbname = "musicfest";

// Create connection
$conn = mysqli_connect($hostname, $username, $password, $dbname);

// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

// Process login form
$username = $_POST['uN'];
$password = $_POST['pW'];

$sql = "SELECT * FROM Users WHERE username = '$username' AND password = '$password'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) == 1) {
  // Login successful
  $row = mysqli_fetch_assoc($result);
  $isOrganizer = $row['Organizer'];
  
  if ($isOrganizer) {
      // User is an organizer, redirect to organizer page
      header("Location: Organizer/index.html");
  } else {
      // User is not an organizer, redirect to regular user page
      header("Location: LoggedIn/index.html");
  }
  exit();
} else {
  // Login failed
  $error_message = "Invalid email or password.";
}


// Close database connection
mysqli_close($conn);
?>
